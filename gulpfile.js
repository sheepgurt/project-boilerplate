var gulp = require('gulp');
var browserSync = require('browser-sync');
var htmlInjector = require('bs-html-injector');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var compression = require('compression');
var connect = require('gulp-connect-php');
var reload = browserSync.reload;

var src = {
    scss: '/app/scss/*.scss',
    css: '/app/css',
    twig: '/app/templates/*.twig',
    php: 'app/*.php'
};

gulp.task('php', function () {
    connect.server({base: 'app', port: 8010, keepalive: true});
});

gulp.task('browser-sync', ['php'], function () {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: false,
        notify: true,
        xip: true
    });
});

// Compile sass into CSS
gulp.task('sass', function () {
    return gulp.src(src.scss)
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(src.css))
        .pipe(reload({stream: true}));
});

gulp.task('default', ['browser-sync'], function () {
    // gulp.watch(['app/*.php'], [reload]);

    gulp.watch(src.scss, ['sass']);
    gulp.watch(src.php).on('change', reload);
    gulp.watch(src.twig).on('change', reload);
});

